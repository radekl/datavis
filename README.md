Datavis is simple web application to create differetn data visualizations connected to 
[Indecol](http://www.ntnu.no/indecol)

The aim is to provide strong REST API support for ability to create multiple graphs.
Examples will be provided with Python and Matlab.

For developers. Flask, MongoDB, phantomjs needs to be installed.

The config file should have at least this setting:

The config.py file must be created at top level with at least this variables:  
SECRET_KEY = 'some_key'  
DEBUG = False  
DEVEL_SERVER = False  
MONGODB_PORT = \<mongodb_port_number_on_production_server\>  
MONGODB_USER = "\<mongodb_username_on_production_server\>"  
MONGODB_PASSW = "\<mongodb_password_on_production_server\>"

For the running on the local machine use DEVEL_SERVER = True.


