"""
creating Flask app

    :copyright: (c) 2016 by Radek Lonka.
    :license: GPLv3, see LICENSE for more details.
"""

from flask import Flask
import logging

from .views.home import home
from .views.nested import nested
from .views.sankey import sankey


app = Flask(__name__)
app.config.from_pyfile('config.py')
app.register_blueprint(home)
app.register_blueprint(nested)
app.register_blueprint(sankey)

file_handler = logging.FileHandler('datavis.log')
file_handler.setLevel(logging.INFO)
app.logger.addHandler(file_handler)
logger = logging.getLogger('application')
logger.addHandler(file_handler)
file_handler.setFormatter(logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
    '[in %(pathname)s:%(lineno)d]'
))
