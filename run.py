"""
This script starts Flask app on development server.
To use it the DEVEL_SERVER variable must be set to True
in the config.py file

    :copyright: (c) 2016 by Radek Lonka.
    :license: GPLv3, see LICENSE for more details.
"""

import os

config_dict = {}
with open(os.path.join(os.path.curdir, 'config.py')) as config:
    for line in config:
        config_dict[line.split('=')[0].replace(' ', '')] = line.split('=')[1].replace(' ', '')

if (config_dict.get('DEVEL_SERVER', False)):
    import sys
    import os
    PACKAGE_PARENT = '..'
    SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
    sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
    from datavis import app
    if __name__ == '__main__':
        app.run()
