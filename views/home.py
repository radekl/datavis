"""
Application to provide easy world map creation and basic configuration
of map design.
The app can be used either by visiting https://datavis.indecol.no
and uploading data csv file with comma or semicolon separated value with no header.
Example:
    FIN, 34.54
    USA, 12.3
    NOR, 13.9

The application also provide API for creating map from JSON.
The usage of API can be found at API_tests folder.

    :copyright: (c) 2016 by Radek Lonka.
    :license: GPLv3, see LICENSE for more details.
"""

from datetime import datetime
from uuid import uuid4
from flask import Blueprint, render_template, jsonify, request, after_this_request, send_file
from flask import current_app as app
from pymongo import MongoClient
import subprocess
import os
import zipfile
import logging
import tempfile
import shutil

from .render_plot_object import create_worldmap_option, create_nestedpie_option

home = Blueprint('home', __name__)


class PlotType:
    WORLD_MAP = 'worldmap'
    NESTED_PIE = 'nestedpie'
    SANKEY = 'sankey'


@home.route('/')
@home.route('/index')
def index():
    return render_template('home/index.html')


@home.route('/plot/<path:type>', methods=["POST"])
def create_worldmap_from_json(type):
    """
    API for creating world map based on JSON input
    :return: json object with status and url of created map if successful or bad request status otherwise
    """
    if request.method == "POST":
        app_config = app.config
        json_dict = request.get_json()
        if not json_dict:
            return jsonify({"status": "conflict in json string"}), 409
        # user only wants to export the map
        if json_dict.get('export'):
            return export_to_file(json_dict, json_dict.get('export'), type)
        else:
            if app_config['DEVEL_SERVER']:
                client = MongoClient()
                db = client['indecol_datavis']
            else:
                client = MongoClient("localhost", app_config['MONGODB_PORT'])
                db = client['indecol_datavis']
                db.authenticate(app_config['MONGODB_USER'], app_config['MONGODB_PASSW'], source='indecol_datavis')
            uid = str(uuid4())
            coll = db['graphs']
            dataset = {"id": uid, "plot_type": type, "dataset": json_dict, "created": datetime.now()}
            coll.insert(dataset)
            if (app_config['DEVEL_SERVER']):
                return jsonify({"status": "created", "url": "http://127.0.0.1:5000/{}".format(uid)}), 201
            else:
                return jsonify({"status": "created", "url": "https://datavis.indecol.no/{}".format(uid)}), 201
    else:
        return jsonify({"status": "bad request"}), 400


@home.route("/<identifier>")
def maps_collection(identifier):
    """
    Route for map created using API
    :param identifier: uid returned together with url using API
    :return: open map template html or status 404 not found it the map is not stored
    """
    app_config = app.config
    if app_config['DEVEL_SERVER']:
        client = MongoClient()
        db = client['indecol_datavis']
    else:
        client = MongoClient("localhost", app_config['MONGODB_PORT'])
        db = client['indecol_datavis']
        db.authenticate(app_config['MONGODB_USER'], app_config['MONGODB_PASSW'], source='indecol_datavis')
    coll = db['graphs']
    dataset = coll.find_one({"id": str(identifier)})
    if dataset:
        if dataset['plot_type'] == PlotType.NESTED_PIE:
            return render_template('/nested.html',
                                   data=dataset['dataset'])
        elif dataset['plot_type'] == PlotType.SANKEY:
            pass
        else:
            return jsonify({"status": "bad request"}), 400
    else:
        return jsonify({"status": "not found"}), 404


@home.route("/<identifier>/export", methods=["GET", "POST"])
def export_map(identifier):
    """
    API for exporting existing graph
    :param identifier: uid of saved map to be exported
    """
    if identifier:
        app_config = app.config
        if app_config['DEVEL_SERVER']:
            client = MongoClient()
            db = client['indecol_datavis']
        else:
            client = MongoClient("localhost", app_config['MONGODB_PORT'])
            db = client['indecol_datavis']
            db.authenticate(app_config['MONGODB_USER'], app_config['MONGODB_PASSW'], source='indecol_datavis')
        coll = db['graphs']
        dataset = coll.find_one({"id": str(identifier)})
        if dataset:
            return export_to_file(dataset['dataset'], dict(), dataset['plot_type'])
        else:
            return jsonify({"status": "not found"}), 404
    else:
        return jsonify({"status": "bad request"}), 400


@home.route("/exportall", methods=["POST"])
def export_all():
    """
    API for exporting all graphs, used in nestedpie ans sankey which has multiple categories
    The result will be zipped file containing one graph per category
    """
    dataset = request.json
    if dataset:
        return export_to_file(dataset, dict(), PlotType.NESTED_PIE)
    else:
        return jsonify({"status": "no data found"}), 404


def export_to_file(dataset, export_conf, type):
    """
    Export map to file
    :param dataset: json file with user configuration and data
    :return: 201 - Success, 500 - Server Error
    """
    input_obj = ''
    if type == PlotType.WORLD_MAP:
        # this is just one world map
        tmp_dir, input_obj = create_worldmap_option(dataset)
    elif type == PlotType.NESTED_PIE:
        # list of nested maps based on number of selectors (categories)
        tmp_dir, input_obj = create_nestedpie_option(dataset)
    elif type == PlotType.SANKEY:
        pass

    app.logger.info('exporting plot type: {}'.format(type))
    image_type = export_conf.get('type', 'jpeg')
    width = export_conf.get('width', 1200)

    try:
        import zlib
        compression = zipfile.ZIP_DEFLATED
    except:
        compression = zipfile.ZIP_STORED

    _, zip_file = tempfile.mkstemp(suffix='.zip', text=True,
                                   dir=tmp_dir)

    with zipfile.ZipFile(zip_file, mode='w') as zf:
        app.logger.info("creating zip file")
        for ix, file in enumerate(input_obj):
            outfile = os.path.join(tmp_dir, 'graph{}.{}'.format(ix, image_type))
            try:
                app.logger.info("running pahntomjs. Currpath: {}".format(os.path.abspath(os.path.curdir)))
                if type == PlotType.WORLD_MAP:
                    subprocess.check_call(
                        ["{}/phantomjs".format(os.path.join(os.path.abspath(os.path.curdir), 'server_export')),
                         "highcharts-convert-maps.js", "-infile", file[0],
                         '-outfile', outfile, '-width', str(width)],
                        cwd=os.path.join(os.path.abspath(os.path.curdir), 'server_export'))
                elif type == PlotType.NESTED_PIE:
                    subprocess.check_call(
                        ["{}/phantomjs".format(os.path.join(os.path.abspath(os.path.curdir), 'server_export')),
                         "highcharts-convert-with-legend.js", "-infile", file[0],
                         '-outfile', outfile, "-legend", file[1], '-width', str(width), '-resources', 'highcharts.js'],
                        cwd=os.path.join(os.path.abspath(os.path.curdir), 'server_export'))
                else:
                    return jsonify({"status": "Can't render map. Server error: Graph not supported yet."}), 500

                zf.write(outfile,
                         'graph{}.{}'.format(ix, image_type),
                         compress_type=compression)

            except subprocess.CalledProcessError as e:
                ret = e.returncode
                app.logger.info("Can't render map. Server error: {}".format(ret))
                return jsonify({"status": "Can't render map. Server error: {}".format(ret)}), 500
            except KeyError as e:
                zf.close()
                app.logger.info("Can't render map. Zipfile error: {}".format(e))
                return jsonify({"status": "Can't zipped file. Zipfile error: {}".format(e)}), 500

    @after_this_request
    def remove_file(response):
        shutil.rmtree(tmp_dir)
        return response

    return send_file(zip_file, mimetype='application/zip')
