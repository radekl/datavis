"""
Application to provide easy world map creation and basic configuration
of map design.
The app can be used either by visiting https://datavis.indecol.no

The application also provide API for creating map from JSON.
The usage of API can be found at API_tests folder.

    :copyright: (c) 2016 by Radek Lonka.
    :license: GPLv3, see LICENSE for more details.
"""

from flask import Blueprint, render_template

sankey = Blueprint('sankey', __name__)


@sankey.route('/sankey')
def home():
    return render_template('sankey.html')