"""
This module is utility to create a string representing specific grpah.
It is then served to phantomJS which render the graph on server.
It is needed when user rendering graph from python/matlab using API.
"""

import tempfile
import os
import colorsys
from colour import Color

def create_worldmap_option(dataset):
    tmp_dir = tempfile.mkdtemp(dir=os.path.join(os.path.abspath(os.path.curdir), 'datavis_temp'))
    _, file = tempfile.mkstemp(suffix='.json', text=True,
                               dir=tmp_dir)
    colors = ''
    if len(dataset['configuration']['colors']) == 2:
        colors += "minColor: '{}', maxColor: '{}'".format(
            dataset['configuration']['colors'][0][1],
            dataset['configuration']['colors'][1][1]
        )
    else:
        colors = ' stops:  ' + str(dataset['configuration']['colors'])

    with open(file, mode="w") as f:
        f.write("""
            {{
                chart: {{
                type: 'map',
                backgroundColor: '{6}'
                }},
                title: {{
                    text: '{1}'
                }},
                subtitle: {{
                    text: '{2}'
                }},
                legend: {{
                    enabled: true,
                    title: {{
                        text: '{3}',
                    }},
                    align: 'left',
                    layout: 'vertical'
                }},
                colorAxis: {{
                    min: {8},
                    max: {9},
                    type: '{4}',
                    {10}
                }},
                xAxis: {{
                   lineWidth: 0,
                   minorGridLineWidth: 0,
                   lineColor: 'transparent',
                   gridLineColor: 'transparent',
                   labels: {{
                       enabled: false
                   }},
                   minorTickLength: 0,
                   tickLength: 0
                }},
                yAxis: {{
                   lineWidth: 0,
                   minorGridLineWidth: 0,
                   lineColor: 'transparent',
                   gridLineColor: 'transparent',
                   labels: {{
                       enabled: false
                   }},
                   minorTickLength: 0,
                   tickLength: 0
                }},
                series: [{{
                    data: {0},
                    mapData: Highcharts.maps['custom/world'],
                    joinBy: ['iso-a3', 'code'],
                    dataLabels: {{
                        enabled: {5},
                        color: '#FFFFFF',
                        format: '{{point.code}}'
                    }},
                    nullColor: '{7}'
                }}],

                    exporting: {{
                   sourceWidth: 1200,
                    sourceHeight: 900
                }}
            }}
            """.format(str(dataset['data']), dataset['configuration']['title'], dataset['configuration']['subtitle'],
                       dataset['configuration']['legendText'], dataset['configuration']['scale'],
                       'true' if dataset['configuration']['showCountryNames'] else 'false',
                       dataset['configuration']['backgroundColor'],
                       dataset['configuration']['NaNColor'], dataset['configuration']['minValue'],
                       dataset['configuration']['maxValue'], colors))
    return tmp_dir, [(file, None)]


def coloured(color, value):
    c = Color(color)
    c.set_hue(c.get_hue() + value)
    c.set_saturation(c.get_saturation() - value)
    return c.hex

def create_nestedpie_option(dataset):
    tmp_dir = tempfile.mkdtemp(dir=os.path.join(os.path.abspath(os.path.curdir), 'datavis_temp'))
    data = dataset["data"]
    export = dataset.get("export", {})
    image_width = export.get("width", 1200)
    ret_files = []
    default_colors = ['#2f7ed8', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a']
    for item in data:
        _, file = tempfile.mkstemp(suffix='.json', text=True,
                                   dir=tmp_dir)
        _, file_html = tempfile.mkstemp(suffix='.js', text=True,
                                   dir=tmp_dir)
        cat1 = []
        cat2 = []
        cat3 = []
        cat4 = []
        cat1_html = ""
        cat2_html = ""
        cat3_html = ""
        cat4_html = ""

        # layer 1 (most inner)
        shif_val = 0.03
        for inx_c1, c1 in enumerate(item['children']):
            color = default_colors[inx_c1]
            c1_val = 0
            color_2_shift = shif_val
            for c2 in c1['children']:
                c2_val = 0
                color_3_shift = shif_val
                for c3 in c2['children']:
                    c3_val = 0
                    color_4_shift = shif_val
                    for c4 in c3['children']:
                        c3_val += c4['value']
                        cat4.append({"name": c4["name"], "y": c4['value'], "color": coloured(color, color_4_shift)})
                        color_4_shift += shif_val
                        cat4_html += '<li class="list-group-item"><canvas id="myCanvas" width="3" height="3" style="border: 3px solid '
                        cat4_html += '{};"></canvas>'.format(coloured(color, color_4_shift))
                        cat4_html += c4["name"]
                        cat4_html += '</li>'
                    cat3.append({"name": c3["name"], "y": c3_val, "color": coloured(color, color_3_shift)})
                    color_3_shift += shif_val
                    c2_val += c3_val
                    cat3_html += '<li class="list-group-item"><canvas id="myCanvas" width="3" height="3" style="border: 3px solid '
                    cat3_html += '{};"></canvas>'.format(coloured(color, color_3_shift))
                    cat3_html += c3["name"]
                    cat3_html += '</li>'
                cat2.append({"name": c2["name"], "y": c2_val, "color": coloured(color, color_2_shift)})
                color_2_shift += shif_val
                c1_val += c2_val
                cat2_html += '<li class="list-group-item"><canvas id="myCanvas" width="3" height="3" style="border: 3px solid '
                cat2_html += '{};"></canvas>'.format(coloured(color, color_2_shift))
                cat2_html += c2["name"]
                cat2_html += '</li>'
            cat1.append({"name": c1["name"], "y": c1_val, "color": color})
            cat1_html += '<li class="list-group-item"><canvas id="myCanvas" width="3" height="3" style="border: 3px solid '
            cat1_html += ';"></canvas>'
            cat1_html += c1["name"]
            cat1_html += '</li>'

        series = """
            [
            {{
                name: '{0}',
                data: {1},
                size: '40%',
                innerSize: '10%',
                dataLabels: {{
                    color: '#ffffff',
                    distance: -30
                }}
            }}, {{
                name: '{2}',
                data: {3},
                size: '60%',
                innerSize: '40%',
                dataLabels: {{}}
            }},
                {{
                    name: '{4}',
                    data: {5},
                    size: '70%',
                    innerSize: '60%',
                    dataLabels: {{}}
                }},
                {{
                    name: '{6}',
                    data: {7},
                    size: '100%',
                    innerSize: '70%',
                }}
                ]
        """.format(dataset["configuration"]["legend_titles"][0], cat1,
                   dataset["configuration"]["legend_titles"][1], cat2,
                   dataset["configuration"]["legend_titles"][2], cat3,
                   dataset["configuration"]["legend_titles"][3], cat4,)

        with open(file_html, mode="w") as html:
            html.write("""
            <div id="legend" style="width: {8}px; height={9}px">
            <ul id="myUl" style="list-style-type:none; text-align: left;">
            <h4>{0}</h4>
            {1}
            <h4>{2}</h4>
            {3}
            <h4>{4}</h4>
            {5}
            <h4>{6}</h4>
            {7}
            </ul>
            </div>
            """.format(dataset["configuration"]["legend_titles"][0], cat1_html,
                       dataset["configuration"]["legend_titles"][1], cat2_html,
                       dataset["configuration"]["legend_titles"][2], cat3_html,
                       dataset["configuration"]["legend_titles"][3], cat4_html,
                       image_width, int((len(cat1_html) + len(cat2_html) + len(cat3_html) + len(cat4_html))/10)))

        with open(file, mode="w") as f:
            f.write("""
                {{
                    chart: {{
                    type: 'pie'
                    }},
                    title: {{
                        text: '{0}'
                    }},
                    subtitle: {{
                        text: '{1}'
                    }},
                    plotOptions: {{
                        pie: {{
                            shadow: false,
                            allowPointSelect: true,
                            dataLabels: {{
                                enabled: false,
                            }},
                            showInLegend: false
                        }}
                    }},
                    series: {2},
                    exporting: {{
                        sourceWidth: 1200,
                        sourceHeight: 900
                    }}
                }}
                """.format(dataset['configuration']['title'], item['name'], series))
        ret_files.append((file, file_html))
    return tmp_dir, ret_files
